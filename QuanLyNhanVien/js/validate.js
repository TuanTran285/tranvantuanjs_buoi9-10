function kiemTraRong(value, idErr) {
    if(value == "") {
        document.getElementById(idErr).innerHTML = "Vui lòng nhập trường này"
        document.getElementById(idErr).style.display = "block"
        return false
    }else {
        document.getElementById(idErr).innerHTML = ""
        return true
    }
}
function kiemTraTrung(taiKhoan, nvArr) {
        let index =  nvArr.findIndex((item) => {
            return taiKhoan == item.taiKhoan
        })
        if(index == -1) {
            document.getElementById("tbTKNV").innerText = ""
            return true
        }else {
            document.getElementById("tbTKNV").innerText = "Tài khoản đã tồn tại"
            document.getElementById("tbTKNV").style.display = "block"
            return false
        }
}

function kiemTraSo(taiKhoan, idErr) {
    var reg = /^-?\d*\.?\d*$/
    let nunber = reg.test(taiKhoan)
    if(nunber) {
        document.getElementById(idErr).innerText = ""
        document.getElementById(idErr).style.display = "block"
        return true
    }else {
        document.getElementById(idErr).innerText = "Trường này phải là số"
        document.getElementById(idErr).style.display = "block"
        return false
    }
}

function kiemTraDoDai(value, idErr, min, max) {
    if(value.length < min || value.length > max) {
        document.getElementById(idErr).innerText = `tài khoản phải từ ${min} đến ${max} ký tự`
        document.getElementById(idErr).style.display = "block"
        return false
    }else {
        document.getElementById("tbTKNV").innerText = ""
        return true
    }
}

function kiemTraChu(value, idErr) {
    const re = /^[^\d]+$/
    let string = re.test(value)
    if(string) {
        document.getElementById(idErr).innerText = ""
        return true
    }else {
        document.getElementById(idErr).innerText = "Trường này phải là chữ"
        document.getElementById(idErr).style.display = "block"
        return false
    }
}

function kiemTraEmail(email) {
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
   const output = re.test(email)
   if(output) {
    document.getElementById("tbEmail").innerHTML = ""
    return true
   }else {
    document.getElementById("tbEmail").innerHTML = "Trường này phải là email"
    document.getElementById("tbEmail").style.display = "block"
    return false
   } 
}

function kiemTraMatKhau(matKhau) {
    const regex = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,}$/;
    const value = regex.test(matKhau)
    if(value) {
        document.getElementById("tbMatKhau").innerText = ""
        return true
    }else {
        document.getElementById("tbMatKhau").innerText = "Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
        document.getElementById("tbMatKhau").style.display = "block"
        return false
    }
}


function chonChucVu(select) {
    switch(select) {
        case "Chọn chức vụ":
            document.getElementById("tbChucVu").innerHTML = "Vui lòng nhập trường này"
            document.getElementById("tbChucVu").style.display = "block"
            return false 
        case "Sếp":
            document.getElementById("tbChucVu").innerHTML = ""
            return true
        case "Trưởng phòng":
            document.getElementById("tbChucVu").innerHTML = ""
            return true
        case "Nhân viên": 
            document.getElementById("tbChucVu").innerHTML = ""
            return true
    }
}


function kiemTraSoGioLam(value, minTime, maxTime) {
    if(value < minTime || value > maxTime) {
        document.getElementById("tbGiolam").innerHTML = `Số giờ làm phải từ ${minTime} đến ${maxTime}`
        document.getElementById("tbGiolam").style.display = "block"
        return false
    }else {
        document.getElementById("tbGiolam").innerHTML = ""
        return true
    }
}