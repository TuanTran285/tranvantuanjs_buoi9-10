let dsnv = [];

// lấy từ localStorage và in ra màn hình
let dsnvJson = localStorage.getItem("DSNV");
 dsnv = JSON.parse(dsnvJson);
dsnv = dsnv.map(item => {
  return new Nhanvien(item.taiKhoan, item.hoTen, item.email, item.matKhau, item.ngayLam, item.luongCoBan, item.chucVu, item.gioLam)
}) 
renderDSNV(dsnv);
function themNhanVien() {
  const nv = layThongTintuForm();
  let invalid =  validateThemNV(nv, dsnv)

  if(invalid) {
    dsnv.push(nv); 
    // lưu xuống localStorage
  let dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", dsnvJson);
  renderDSNV(dsnv);
  $('#myModal').modal('hide')
}
}

function xoaNhanVien(taiKhoanSv) {
  const viTri = timViTri(taiKhoanSv, dsnv)
  dsnv.splice(viTri, 1);
  localStorage.setItem("DSNV", JSON.stringify(dsnv));
  renderDSNV(dsnv);
}

function suaNhanVien(taiKhoanNV) {
  $('#myModal').modal('show')
  const viTri = timViTri(taiKhoanNV, dsnv)
  capNhatThongTinLenForm(dsnv[viTri])
  // disabled thêm người dùng
  document.getElementById("btnThemNV").disabled = true
  // delete disabled cập nhật
  document.getElementById("btnCapNhat").disabled = false

  // disabled id nhân viên
  document.getElementById("tknv").disabled = true

}

function capNhatNv() {
  const nv = layThongTintuForm()
  const viTri = timViTri(nv.taiKhoan, dsnv)
  if(viTri != -1) {
    let invalid = validateCapNhatNV(nv)
  if(invalid) {
    dsnv[viTri] = nv
    renderDSNV(dsnv)
    localStorage.setItem("DSNV", JSON.stringify(dsnv));
    $('#myModal').modal('hide')
  }
}
}


function resetForm() {
  const form = document.getElementById("form")
  form.reset()
}



// disabled = false cái thêm nhân viên
function btnClickNV() {
  // khi click vào thi disable cập nhật và hiện thêm người dùng
  document.getElementById("btnThemNV").disabled = false
  document.getElementById("btnCapNhat").disabled = true
  
  document.getElementById("tknv").disabled = false


  // khi click vào thì resest
  resetForm()
}




// tìm loại nhân viên

function timLoaiNhanVien() {
  const valueInput = document.getElementById("searchName").value
  let contentHTML = ""
  for(let i = 0; i<dsnv.length; i++) {
    const item = dsnv[i]
    if(valueInput.trim().toLowerCase() == item.xepLoai().toLowerCase()) {
      let content = `<span class="text-info h6">${item.hoTen},</span> `
      contentHTML += content
    }
  }
  document.getElementById("tbTimTenNV").innerHTML = contentHTML

  if(contentHTML == "") {
    document.getElementById("tbTimTenNV").innerHTML = `<spann class="text-danger h5">Không tìm thấy</spann>`
  }
} 