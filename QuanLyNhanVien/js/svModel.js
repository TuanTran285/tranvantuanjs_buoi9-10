function Nhanvien(_taiKhoan, _hoTen, _email, _matKhau, _ngayLam, _luongCoBan, _chucVu, _gioLam) {
    this.taiKhoan = _taiKhoan
    this.hoTen = _hoTen
    this.email = _email
    this.matKhau = _matKhau
    this.ngayLam = _ngayLam
    this.luongCoBan = _luongCoBan
    this.chucVu = _chucVu
    this.gioLam = _gioLam
    this.timChucVu = function() {
        switch(this.chucVu) {
        case "Sếp":
            return 3
        case "Trưởng phòng":
            return 2
        case "Nhân viên": 
            return 1
        }
    }
    this.tongLuong = function() {
        const chucVu = this.timChucVu()
        return (this.luongCoBan  * chucVu).toLocaleString()
    }
    this.xepLoai = function() {
        const gioLam = this.gioLam
        if(gioLam >= 180) {
            return "Xuất sắc"
        }else if(gioLam >= 176) {
            return "Giỏi"
        }else if(gioLam >= 160) {
            return "Khá"
        }else {
            return "Trung Bình"
        }
    }
}
