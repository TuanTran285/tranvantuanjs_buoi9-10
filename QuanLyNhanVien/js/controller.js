function layThongTintuForm() {
    const _taiKhoan = document.getElementById("tknv").value
    const _hoTen = document.getElementById("name").value
    const _email = document.getElementById("email").value
    const _matKhau = document.getElementById("password").value
    const _ngayLam = document.getElementById("datepicker").value
    const _luongCoBan = document.getElementById("luongCB").value * 1
    const _chucVu = document.getElementById("chucvu").value
    const _gioLam = document.getElementById("gioLam").value * 1
    
    const nv = new Nhanvien(_taiKhoan, _hoTen, _email, _matKhau, _ngayLam, _luongCoBan, _chucVu, _gioLam)
    return nv
}


function renderDSNV(nvArr) {
    let contentHTML = ""
    for(let i = 0; i<nvArr.length; i++) {
        const item = nvArr[i]
        let contentTr = `
            <tr>
                <td>${item.taiKhoan}</td>
                <td>${item.hoTen}</td>
                <td>${item.email}</td>
                <td>${item.ngayLam}</td>
                <td>${item.chucVu}</td>
                <td>${item.tongLuong()}</td>
                <td>${item.xepLoai()}</td>
                <td><button class="btn btn-danger" onclick="xoaNhanVien('${item.taiKhoan}')">Xóa</button>
                <button class="btn btn-info mt-2" onclick="suaNhanVien('${item.taiKhoan}')">Sửa</button></td>
            </tr>
        `
        contentHTML += contentTr
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML
}

function timViTri(taiKhoanNv, dsnv) {
    let viTri = -1;
    for (let i = 0; i < dsnv.length; i++) {
      if (dsnv[i].taiKhoan == taiKhoanNv) {
        viTri = i;
      }
    }
    return viTri
}


function capNhatThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.taiKhoan 
    document.getElementById("name").value = nv.hoTen
    document.getElementById("email").value = nv.email
    document.getElementById("password").value = nv.matKhau
    document.getElementById("datepicker").value = nv.ngayLam
    document.getElementById("luongCB").value = nv.luongCoBan
    document.getElementById("chucvu").value = nv.chucVu
    document.getElementById("gioLam").value = nv.gioLam
}




function validateThemNV (nv, dsnv) {
   // kiểm tra tài khoản
  invalid = kiemTraRong(nv.taiKhoan, "tbTKNV") && kiemTraTrung(nv.taiKhoan, dsnv) && kiemTraSo(nv.taiKhoan, "tbTKNV") && kiemTraDoDai(nv.taiKhoan, "tbTKNV", 4, 6)
  // kiểm tra tên nhân viên
    invalid = invalid & (kiemTraRong(nv.hoTen, "tbTen") && kiemTraChu(nv.hoTen, "tbTen"))
  // kiểm tra email
    invalid = invalid & (kiemTraRong(nv.email, "tbEmail") && kiemTraEmail(nv.email))
    // kiểm tra mật khẩu
    invalid = invalid & (kiemTraRong(nv.matKhau, "tbMatKhau") && kiemTraMatKhau(nv.matKhau) && kiemTraDoDai(nv.matKhau, "tbMatKhau", 6, 10))
    // kiểm tra ngày làm
    invalid = invalid & kiemTraRong(nv.ngayLam, "tbNgay")
    // chức vụ
    invalid = invalid & chonChucVu(nv.chucVu)
    // lương cơ bản
    invalid = invalid & (kiemTraRong(nv.luongCoBan, "tbLuongCB") && kiemTraSo(nv.luongCoBan, "tbLuongCB"))
    // giờ làm 
    invalid = invalid & (kiemTraRong(nv.gioLam, "tbGiolam") && kiemTraSo(nv.gioLam, "tbGiolam") && kiemTraSoGioLam(nv.gioLam, 80, 200))
    return invalid
}
function validateCapNhatNV (nv) {
    // kiểm tra tài khoản
   invalid = kiemTraRong(nv.taiKhoan, "tbTKNV") && kiemTraSo(nv.taiKhoan, "tbTKNV") && kiemTraDoDai(nv.taiKhoan, "tbTKNV", 4, 6)
   // kiểm tra tên nhân viên
     invalid = invalid & (kiemTraRong(nv.hoTen, "tbTen") && kiemTraChu(nv.hoTen, "tbTen"))
   // kiểm tra email
     invalid = invalid & (kiemTraRong(nv.email, "tbEmail") && kiemTraEmail(nv.email))
     // kiểm tra mật khẩu
     invalid = invalid & (kiemTraRong(nv.matKhau, "tbMatKhau") && kiemTraMatKhau(nv.matKhau) && kiemTraDoDai(nv.matKhau, "tbMatKhau", 6, 10))
     // kiểm tra ngày làm
     invalid = invalid & kiemTraRong(nv.ngayLam, "tbNgay")
     // chức vụ
     invalid = invalid & chonChucVu(nv.chucVu)
     // lương cơ bản
     invalid = invalid & (kiemTraRong(nv.luongCoBan, "tbLuongCB") && kiemTraSo(nv.luongCoBan, "tbLuongCB"))
     // giờ làm 
     invalid = invalid & (kiemTraRong(nv.gioLam, "tbGiolam") && kiemTraSo(nv.gioLam, "tbGiolam") && kiemTraSoGioLam(nv.gioLam, 80, 200))
     return invalid
 }

